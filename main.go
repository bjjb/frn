package main

import (
	"fmt"

	"github.com/spf13/cobra"
)

var frn = &cobra.Command{
	Use:   "frn",
	Short: "Tools to deal with the team resources",
	Long: `A command-line application to help monitor services, manage
resources, repos, etc.`,
	Version: Version,
	Run: func(c *cobra.Command, args []string) {
		fmt.Println("Hey")
	},
}

func main() {
	frn.Execute()
}

//go:generate ./version
