FoiReanN
========

A small command-line program to help organize and maintain an engineering
team's infrastructure and whatnot. ("Foireann" just means "team" in Irish.)

Installation
------------

    go install gitlab.com/bjjb/frn

(you'll need Go installed). Or just clone the repo, and run `go install`
therein.

Usage
-----

There's no formal functional specification to describe (yet), so just get the
latest version and run `frn help` for documentation.

Development
-----------

- Make sure you test everything
- Keep PRs tidy and self-explanatory (the PR description should be usable as
    an entry in a release-note).
- Use semantic version, and tag releases.
